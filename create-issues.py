#!/usr/bin/env python3

import argparse
import json
import os
import requests
import subprocess

parser = argparse.ArgumentParser(description='Create gitlab issues from Pick-to: commits.')
parser.add_argument('--fast', help='Skip looking for commits after we found a commit for which an issue has already been created', action="store_true")
args = parser.parse_args()

gitlab_token = os.environ.get('GITLAB_TOKEN')

if not gitlab_token:
    print("No GITLAB_TOKEN env var found")
    quit()

qt5dir = os.environ.get('QT5_DIR')

if not qt5dir:
    print("No QT5_DIR env var found")
    quit()

gitlab_project_id = 7761

qt5submodules = subprocess.run(['git', 'submodule'], cwd=qt5dir, capture_output=True).stdout

for qt5submodule in qt5submodules.splitlines():
    qt5submodule = qt5submodule.split()[1].decode("latin1")
    submodule_path = qt5dir + qt5submodule
    print(qt5submodule)
    subprocess.run(['git', 'fetch'], cwd=submodule_path)
    git_log = subprocess.run(['git', 'log', '--grep=Pick-to:.*5\.15.*', '--grep=Pick-to:.*5\.12.*', '--pretty=oneline', 'origin/dev'], cwd=submodule_path, capture_output=True).stdout
    for line in git_log.splitlines():
        commit_hash = line.split()[0].decode("latin1")
        git_log_backport = subprocess.run(['git', 'log', '--grep=cherry picked from commit ' + commit_hash, 'origin/kde/5.15'], cwd=submodule_path, capture_output=True).stdout
        if not git_log_backport:
            title = qt5submodule + ": " + line.decode("utf-8")[41:]
            title_to_search = title.replace("<cctype>", "");
            description = "https://invent.kde.org/qt/qt/"+qt5submodule+"/-/commit/"+commit_hash
            r = requests.get("https://invent.kde.org/api/v4/projects/7761/issues", 
                             data = {'search' : title_to_search},
                             headers={"PRIVATE-TOKEN" : gitlab_token})
            if not r.ok:
                print("Failed searching for already existing issue!")
                quit()
            
            create_issue = True
            for search_result in json.loads(r.content):
                if search_result["title"] == title and search_result["description"] == description:
                    create_issue = False
            
            if create_issue:             
                r = requests.post("https://invent.kde.org/api/v4/projects/7761/issues?title=test",
                                  data = {'title' : title, "description" : description, "labels" : qt5submodule},
                                  headers={"PRIVATE-TOKEN" : gitlab_token})
                if not r.ok:
                    print("Creating issue for " + commit_hash + " failed!")
                    quit()
                
                print("Created new issue: " + title)
            elif args.fast:
                break
                
